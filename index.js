// Cubing
let num = 5;
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);


// Address
let address = [21, "P.Oliveros Street", "Mandaluyong City", 1550];
let [streetNumber,streetName,city,zipCode] = address;
console.log(`I live at ${streetNumber} ${streetName}, ${city} ${zipCode}`);

let animal = {
	name: "Yuki",
	species:"Siberian Husky",
	weight: 22,
	heigth: 20
};

// Animal
let {name,species,weight,heigth} = animal
console.log(`${name} was a ${species}. She weighed at ${weight} kgs and was ${heigth} inches tall`);

// List Numbers
let numbers = [1,2,3,4,5];

numbers.forEach((number) => {
	console.log(number);
});


// Sum of numbers
let reduceNumber = numbers.reduce((number, total) => number + total);
console.log(reduceNumber); 



// Dog

class Dog{
	constructor(name2,age,breed){
		this.name2 = name2;
		this.age = age;
		this.breed = breed;
	}
};


let newDog = new Dog("Rusty", 7, "Rottweiler");
console.log(newDog);